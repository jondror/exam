<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m160722_140758_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => 'string',
			'password' => 'string',
			'auth_key' => 'string'
			
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
