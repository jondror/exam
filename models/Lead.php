<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $notes
 * @property integer $status
 * @property integer $owner
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
     public function rules()
    {
		$rules = []; 
		$stringItems = [['notes'], 'string'];
		$integerItems  = ['status', 'created_at', 'updated_at', 'created_by', 'updated_by'];		
		if (\Yii::$app->user->can('updateLead')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems = [['name', 'email', 'phone'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'notes' => 'Notes',
            'status' => 'Status',
            'owner' => 'Owner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public function getFullname(){
		return $this->name;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function getUserOwner(){
		
		return $this->hasOne(User::className(), ['id' => 'owner']);
	}
	
	public function getCreatedby(){
		
		return $this->hasOne(User::className(), ['id' => 'created_by']);
	}
	
	public function getUpdatedby(){
		
		return $this->hasOne(User::className(), ['id' => 'updated_by']);
	}
	
	public function getStatusItem(){
		
		return $this->hasOne(Status::className(), ['id' => 'status']);
	}
	public static function getLeads()
	{
		$allLeads = self::find()->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
		return $allLeadsArray;						
	}
	
	//Get a list of leads that have made deals
	public static function getLeadsWithDeals()
	{
		$leads = self::getLeads();
		//Check if lead has any deals. If not, it is unset from the array		
		foreach ($leads as $key => $lead) {
			if (Deal::findDealFromLead($key)==NULL) {
				unset($leads[$key]);
			}
		}
		
		
		$leads[-1] = 'All Leads';
		$leads = array_reverse ( $leads, true );
		return $leads;	
	}
	
	//Find all deals made by a single Lead
	public static function getAllDeals($leadId){
		$deals=Deal::findAll([
			'leadId' => $leadId,
		]);
		
		$allLeadsArray = ArrayHelper::
					map($deals,'id', 'name');
		return $allLeadsArray;
	}
}
