<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property double $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'name', 'amount'], 'required'],
            [['leadId'], 'integer'],
            [['amount'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	
	public function getDealLead(){
		
		return $this->hasOne(Lead::className(), ['id' => 'leadId']);
	}
	
	public static function findDealFromLead($leadId)
    {
        return static::find()
		->where(['leadId'=>$leadId])
		->one();
    }
}
