<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller
{
    public function actionView($id)
    {
        $model = Student::findOne($id);
		$name = $model->name;
		return $this->render('showOne' , ['name' => $name]);
    }
	
	public function actionAdd(){
		$student= new Student();
		$student->name='Yoni';
		$student->save();
		
		$student= new Student();
		$student->name='Rotem';
		$student->save();
		
		$student= new Student();
		$student->name='David';
		$student->save();
		
		$student= new Student();
		$student->name='Tal';
		$student->save();
		
		return $this->goHome();
	}
}