<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacthreeController extends Controller
{

	public function actionPermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createDeal = $auth->createPermission('createDeal');
		$createDeal->description = 'Team Leader can create deals';
		$auth->add($createDeal);
		
		$deleteDeal = $auth->createPermission('deleteDeal');
		$deleteDeal->description = 'Team Leader can delete deals';
		$auth->add($deleteDeal);
		
		$updateDeal = $auth->createPermission('updateDeal');
		$updateDeal->description = 'Team Leader can update deals';
		$auth->add($updateDeal);		
	}


	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teamleader = $auth->getRole('teamleader');

		$updateDeal = $auth->getPermission('updateDeal');
		$auth->addChild($teamleader, $updateDeal);
		
		$deleteDeal = $auth->getPermission('deleteDeal');
		$auth->addChild($teamleader, $deleteDeal);
		
		$createDeal = $auth->getPermission('createDeal');
		$auth->addChild($teamleader, $createDeal);
	}
}