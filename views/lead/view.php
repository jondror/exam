<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php if (\Yii::$app->user->can('updateLead') || 
		    \Yii::$app->user->can('updateOwnLead', ['lead' =>$model]) ) { ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			<?php } ?>
		<?php if (\Yii::$app->user->can('updateLead')){ ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            'phone',
            'notes:ntext',
			[ // the status name
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,			
			],	
			[ // the owner name of the lead
				'label' => $model->attributeLabels()['owner'],
				'format' => 'html',
				'value' => Html::a($model->userOwner->fullname, 
					['user/view', 'id' => $model->userOwner->id]),			
			],
			[ // created_at
				'label' => $model->attributeLabels()['created_at'],
				'format' => 'html',
				'value' => isset($model->created_at) ? date('d/m/y H:i:s' ,$model->created_at) : "No Data",			
			],
			[ // updated_at
				'label' => $model->attributeLabels()['updated_at'],
				'format' => 'html',
				'value' => isset($model->updated_at) ? date('d/m/y H:i:s' ,$model->updated_at) : "No Data",			
			],
            [ // created By
				'label' => $model->attributeLabels()['created_by'],
				'format' => 'html',
				'value' => isset($model->createdby->fullname) ? (Html::a($model->createdby->fullname, 
					['user/view', 'id' => $model->createdby->id])) : "No Data" ,			
			],
			[ // updated By
				'label' => $model->attributeLabels()['updated_by'],
				'format' => 'html',
				'value' => isset($model->updatedby->fullname) ? (Html::a($model->updatedby->fullname, 
					['user/view', 'id' => $model->updatedby->id])) : "No Data" ,			
			],
			
        ],
    ]) ?>
	<table>
	<tr>
		<th>Deals made by this Lead</th>
	</tr>
	<?php
	if (!empty($allDeals)){
		foreach($allDeals as $id => $name){
		echo '<tr><td>';	
		echo Html::a('Deal No.'.$id.': '.$name, ['deal/view', 'id' => $id]);
		echo '</td></tr>';
		}
	}
	else{
		echo "<tr><td>No Deals For this Lead</td></tr>";
	}

	?>
	</table>
	

</div>
